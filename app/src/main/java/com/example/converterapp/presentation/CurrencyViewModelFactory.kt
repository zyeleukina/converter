package com.example.converterapp.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.converterapp.domain.interactor.MainInteractor

//class CurrencyViewModelFactory(private val interactor: MainInteractor) : ViewModelProvider.Factory {
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(CurrencyViewModel::class.java)) {
//            return CurrencyViewModel(interactor) as T
//        }
//        throw IllegalArgumentException("Unknown ViewModel class")
//    }
//}