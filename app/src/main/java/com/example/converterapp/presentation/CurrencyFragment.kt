package com.example.converterapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.converterapp.R
import com.example.converterapp.data.CurrencyAdapter
import com.example.converterapp.domain.interactor.MainInteractor
import com.example.converterapp.domain.repository.CurrencyRepository
import com.example.converterapp.domain.usecases.GetConvertionResultUseCase
import com.example.converterapp.domain.usecases.GetCurrenciesUseCase
import com.example.converterapp.presentation.mapper.CurrencyMapper

class CurrencyFragment : Fragment() {

    private val viewModel: CurrencyViewModel by viewModels()
    private val currencyAdapter = CurrencyAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_currency, container, false)

        val currencyListRecyclerView = view.findViewById<RecyclerView>(R.id.currencyListRecyclerView)

        val layoutManager = LinearLayoutManager(requireContext())
        currencyListRecyclerView.layoutManager = layoutManager
        currencyListRecyclerView.adapter = currencyAdapter

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getFluctuation()
        viewModel.currencies.observe(viewLifecycleOwner) {
            currencyAdapter.submitList(CurrencyMapper().mapToPresentation(it))
        }
    }
}