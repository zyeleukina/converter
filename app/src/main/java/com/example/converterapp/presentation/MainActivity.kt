package com.example.converterapp.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.converterapp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, CurrencyFragment())
            .commit()

        findViewById<Button>(R.id.openBtn).setOnClickListener {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, ConvertCurrencyFragment())
                .addToBackStack(null)
                .commit()
        }
    }
}