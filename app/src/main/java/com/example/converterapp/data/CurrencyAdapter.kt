package com.example.converterapp.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.converterapp.R
import com.example.converterapp.domain.entities.Currency

class CurrencyAdapter : ListAdapter<Currency, CurrencyAdapter.CurrencyViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.currency_content, parent, false)
        return CurrencyViewHolder(view)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val currencyItem = getItem(position)
        holder.bind(currencyItem)
    }

    inner class CurrencyViewHolder(private val view: View) :
        RecyclerView.ViewHolder(view) {


        private val currencyName: TextView = view.findViewById(R.id.currencyName)
        private val currentRate: TextView = view.findViewById(R.id.currentRate)
        private val rateChange: TextView = view.findViewById(R.id.rateChange)

        fun bind(currencyItem: Currency){
            currencyName.text = currencyItem.name
            currentRate.text = currencyItem.end_rate.toString()
            rateChange.text = currencyItem.change.toString()
            view.setBackgroundColor(ContextCompat.getColor(view.context, currencyItem.backgroundColorResId))
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<Currency>() {
        override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
            return oldItem.change == newItem.change
        }

        override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
            return oldItem == newItem
        }
    }
}
