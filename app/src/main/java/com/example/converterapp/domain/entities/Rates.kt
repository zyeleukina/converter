package com.example.converterapp.domain.entities

data class Rates(
    val base: String,
    val end_date: String,
    val fluctuation: Boolean,
    val rates: Map<String, Currency>
)