package com.example.converterapp.domain.usecases

import com.example.converterapp.domain.entities.Rates
import com.example.converterapp.domain.repository.CurrencyRepository

class GetCurrenciesUseCase{
    private val repository = CurrencyRepository

    suspend fun getCurrencies(startDate: String, endDate: String): Rates{
        return repository.getFluctuation(startDate, endDate)
    }
}